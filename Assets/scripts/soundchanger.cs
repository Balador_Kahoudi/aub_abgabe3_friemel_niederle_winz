﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class soundchanger : MonoBehaviour
{
    public GameObject Runner;
    public List<AudioClip> sounds;
    public List<AudioClip> sounds_alt;
    private int current_sound=5;
    private int last_Sound_type = 0;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(test());
    }

    // Update is called once per frame
    void Update()
    {
    }

    bool Ground_checker()
    {
        if (Runner.transform.position.z < 0)
        {
            if (Runner.transform.position.x < 0)
            {
                if (current_sound != 0)
                {
                    current_sound = 0;
                    return true;
                }
            }
            else
            {
                if (current_sound != 1)
                {
                    current_sound = 1;
                    return true;
                }
            }
        }
        else
        {
            if (Runner.transform.position.x > 0)
            {
                if (current_sound != 2)
                {
                    current_sound = 2;
                    return true;
                }
            }
            else
            {
                if (current_sound != 3)
                {
                    current_sound = 3;
                    return true;
                }
            }
        }
        return false;
    }

    void Sound_changer()
    {
        Runner.GetComponent<AudioSource>().clip = sounds[current_sound];
        Runner.GetComponent<AudioSource>().Play();
        last_Sound_type = 0;
    }

    void Sound_switch()
    {
        if (last_Sound_type == 0)
        {
            Runner.GetComponent<AudioSource>().clip = sounds_alt[current_sound];
            Runner.GetComponent<AudioSource>().Play();
            last_Sound_type = 1;
        }
        else
        {
            Runner.GetComponent<AudioSource>().clip = sounds[current_sound];
            Runner.GetComponent<AudioSource>().Play();
            last_Sound_type = 0;
        }
    }

    private IEnumerator test()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);
            Debug.Log("Jetzt");
            if (Ground_checker())
            {
                Sound_changer();
            }
            else
            {
                Sound_switch();
            }
            Debug.Log(current_sound);

        }
        yield return null;
    }
}
