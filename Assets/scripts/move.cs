﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour
{
    public GameObject Runner;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(test());
    }

    // Update is called once per frame
    void Update()
    {
    }
    

    private IEnumerator test()
    {
        int i = 1;
        while (true)
        {
            yield return new WaitForSeconds(0.1f);
            Debug.Log("Jetzt" + i);
            if (i == 101)
            {
                Runner.transform.Rotate(Runner.transform.rotation.x, Runner.transform.rotation.y + 90, Runner.transform.rotation.z);
            }
            if (i == 201)
            {
                Runner.transform.Rotate(Runner.transform.rotation.x, Runner.transform.rotation.y + 90, Runner.transform.rotation.z);
            }
            if (i == 301)
            {
                Runner.transform.Rotate(Runner.transform.rotation.x, Runner.transform.rotation.y + 90, Runner.transform.rotation.z);
            }
            if (i == 401)
            {
                Runner.transform.Rotate(Runner.transform.rotation.x, Runner.transform.rotation.y + 90, Runner.transform.rotation.z);
                i = 1;
            }
            if (i <= 100)
            {
                Runner.transform.position = new Vector3(Runner.transform.position.x, Runner.transform.position.y, Runner.transform.position.z + 0.1f);
            }
            if (100 < i && i <= 200)
            {
                Runner.transform.position = new Vector3(Runner.transform.position.x + 0.1f, Runner.transform.position.y, Runner.transform.position.z);
            }
            if (200 < i && i <= 300)
            {
                Runner.transform.position = new Vector3(Runner.transform.position.x, Runner.transform.position.y, Runner.transform.position.z - 0.1f);
            }
            if (300 < i)
            {
                Runner.transform.position = new Vector3(Runner.transform.position.x - 0.1f, Runner.transform.position.y, Runner.transform.position.z);
            }
            i++;
        }
        yield return null;
    }
}